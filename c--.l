%{ /* Codigo C */
  #include <stdlib.h>
  #include <string.h>
  #include "c--.tab.h"
  int linea = 1;
%}
	
BLANCO_TAB [ \t]+

SALTO_LINEA "\n"

INICIO_IDENTIFICADOR ([[:alpha:]]|"_")

IDENTIFICADOR	{INICIO_IDENTIFICADOR}+([[:alnum:]]|'_')*

CONSTANTE_OCTAL	0[0-7]

CONSTANTE_DECIMAL	[[:digit:]]

CONSTANTE_HEX	0[xX][0-9a-fA-F]+

CONSTANTE_ENTERA {CONSTANTE_OCTAL}+|{CONSTANTE_DECIMAL}+|{CONSTANTE_HEX}

CONSTANTE_REAL_HEX 0[xX][0-9a-fA-F]*"."[0-9a-fA-F]+

CONSTANTE_REAL_DECIMAL [[:digit:]]*"."[[:digit:]]+

EXP	[Ee][+-]?{CONSTANTE_DECIMAL}

MANTISA {CONSTANTE_DECIMAL}+|{CONSTANTE_HEX}|{CONSTANTE_REAL_HEX}|{CONSTANTE_REAL_DECIMAL}

CONSTANTE_REAL_MANTISA_EXP	{MANTISA}{EXP}

CONSTANTE_REAL	{CONSTANTE_REAL_HEX}|{CONSTANTE_REAL_DECIMAL}|{CONSTANTE_REAL_MANTISA_EXP}

CONSTANTE_CARACTER_ESCAPADA \\\'|\\\"|\\\?|\\\\|\\a|\\b|\\f|\\n|\\r|\\t|\\v

CARACTER_OCTAL	\\([0-3][0-7][0-7]|[0-7][0-7]|[0-7])

CARACTER_HEX \\[Xx][0-9a-fA-F]{1,2}

CONSTANTE_CARACTER \'([^[\\'][\n]]|{CARACTER_OCTAL}|{CONSTANTE_CARACTER_ESCAPADA}|{CARACTER_HEX})\'

CADENA \"(\\.|[^"])*
FINCADENA \"

%x cadena

LIBRERIA \"<[^\n>\"]+>\"


%x comentario

COMENTARIO_LINEA "//"[^\n]*

%%
{BLANCO_TAB}


"sizeof" {printf("%s\n",yytext);return OPERADOR_SIZEOF;}

"?" {printf("%s\n", yytext); return INTERROGANTE;}

"=" {printf("%s\n",yytext);return ASIGNACION_IGUAL;}
"*=" {printf("%s\n",yytext);return ASIGNACION_MULT;}
"/=" {printf("%s\n",yytext);return ASIGNACION_DIV;}
"%=" {printf("%s\n",yytext);return ASIGNACION_MOD;}
"+=" {printf("%s\n",yytext);return ASIGNACION_MAS;}
"-=" {printf("%s\n",yytext);return ASIGNACION_MENOS;}
"<<=" {printf("%s\n",yytext);return ASIGNACION_DESP_IZQ;}
">>=" {printf("%s\n",yytext);return ASIGNACION_DESP_DER;}
"&="  {printf("%s\n",yytext);return ASIGNACION_AMB;}
"^="  {printf("%s\n",yytext);return ASIGNACION_XOR;}
"|="  {printf("%s\n",yytext);return ASIGNACION_OR;}


"--" {printf("%s\n",yytext);return ARITMETICO_MENOSMENOS;}
"++" {printf("%s\n",yytext);return ARITMETICO_MASMAS;}


">" {printf("%s\n",yytext);return RELACIONAL_MAYOR;}
"<" {printf("%s\n",yytext);return RELACIONAL_MENOR;}
"<="  {printf("%s\n",yytext);return RELACIONAL_MENOR_IGUAL;}
">="  {printf("%s\n",yytext);return RELACIONAL_MAYOR_IGUAL;}
"=="  {printf("%s\n",yytext);return RELACIONAL_IGUAL;}
"!="  {printf("%s\n",yytext);return RELACIONAL_DISTINTO;}


"->" {printf("%s\n",yytext);return MEMORIA_FLECHA;}

"+" {printf("%s\n",yytext);return SIGNO_MAS;}
"-" {printf("%s\n",yytext);return SIGNO_MENOS;}
"*" {printf("%s\n",yytext);return SIGNO_POR;}
"/" {printf("%s\n",yytext);return SIGNO_DIV;}
"%" {printf("%s\n",yytext);return SIGNO_MOD;}
"(" {printf("%s\n",yytext);return PARIZQ;}
")" {printf("%s\n",yytext);return PARDER;}
"[" {printf("%s\n",yytext);return CORCHETE_IZQ;}
"]" {printf("%s\n",yytext);return CORCHETE_DER;}
"{" {printf("%s\n",yytext);return LLAVE_IZQ;}
"}" {printf("%s\n",yytext);return LLAVE_DER;}
"~" {printf("%s\n",yytext);return GUSANITO;}
"!" {printf("%s\n",yytext);return NEGACION_LOG;}
"#" {printf("%s\n",yytext);return SOSTENIDO;}



"int" {printf("%s\n",yytext);return INT;}
"float" {printf("%s\n",yytext);return FLOAT;}
"double" {printf("%s\n",yytext);return DOUBLE;}
"char" {printf("%s\n",yytext);return CHAR;}
"unsigned" {printf("%s\n",yytext);return UNSIGNED;}
"signed" {printf("%s\n",yytext);return SIGNED;}
"void" {printf("%s\n",yytext);return VOID;}
"long" {printf("%s\n",yytext);return LONG;}
"short" {printf("%s\n",yytext);return SHORT;}
"typedef" {printf("%s\n",yytext);return TYPEDEF;}
"define" {printf("%s\n",yytext);return DEFINE;}
"include" {printf("%s\n",yytext);return INCLUDE;}

"&&" {printf("%s\n",yytext);return AND_LOGICO;}
"||" {printf("%s\n",yytext);return OR_LOGICO;}

"&" {printf("%s\n",yytext);return AND_BINARIO;}
"^" {printf("%s\n",yytext);return XOR_BINARIO;}
"|" {printf("%s\n",yytext);return OR_BINARIO;}

"<<" {printf("%s\n",yytext);return DESP_IZQ;}
">>" {printf("%s\n",yytext);return DESP_DER;}

","  {printf("%s\n",yytext);return COMA;}
"." {printf("%s\n",yytext);return PUNTO;}
";" {printf("%s\n",yytext);return PUNTO_Y_COMA;}
":" {printf("%s\n",yytext);return DOS_PUNTOS;}


"if" {printf("%s\n",yytext);return IF;}
"else" {printf("%s\n",yytext);return ELSE;}
"switch" {printf("%s\n",yytext);return SWITCH;}
"case" {printf("%s\n",yytext);return CASE;}
"default" {printf("%s\n",yytext);return DEFAULT;}
"while" {printf("%s\n",yytext);return WHILE;}
"do" {printf("%s\n",yytext);return DO;}
"for" {printf("%s\n",yytext);return FOR;}
"goto" {printf("%s\n",yytext);return GOTO;}
"continue" {printf("%s\n",yytext);return CONTINUE;}
"break" {printf("%s\n",yytext);return BREAK;}
"return" {printf("%s\n",yytext);return RETURN;}

"extern" {printf("%s\n",yytext);return EXTERN;}
"static" {printf("%s\n",yytext);return STATIC;}
"auto" {printf("%s\n",yytext);return AUTO;}

"struct" {printf("%s\n",yytext);return STRUCT;}
"union" {printf("%s\n",yytext);return UNION;}


"/*" BEGIN(comentario);
<comentario>[^*\n]* {}
<comentario>"*"+[^*/\n]* {}
<comentario>{SALTO_LINEA} {linea++;} 
<comentario>"*"+"/" {
			BEGIN(INITIAL);
		    }



{LIBRERIA} {
                                  yylval.texto = (char *) malloc (strlen(yytext) + 1);
                                  strcpy(yylval.texto, yytext);
                                  printf("%s\n",yytext);
                                  return (LIBRERIA);
                                  }


{CONSTANTE_ENTERA}	{
                                  yylval.valor_entero = atoi(yytext);
                                  printf("%s\n",yytext);
                                  return (CONSTANTE_ENTERA);
                                  }


{CONSTANTE_REAL}	{
                                  yylval.valor_real = atof(yytext);
                                  printf("%s\n",yytext);
                                  return (CONSTANTE_REAL);
                                  
                                  }


{CONSTANTE_CARACTER}	{
                                  yylval.caracter = yytext[0];
                                  printf("%s\n",yytext[0]);
                                  return (CONSTANTE_CARACTER);
                                  }

{CADENA}{FINCADENA} {
                                  yylval.texto = (char *) malloc (strlen(yytext) + 1);
                                  strcpy(yylval.texto, yytext);
                                  printf("%s\n",yytext);
                                  return (CONSTANTE_CADENA);
                                  }

{COMENTARIO_LINEA}

{IDENTIFICADOR}	{ 
                                  yylval.texto = (char *) malloc (strlen(yytext) + 1);
                                  strcpy(yylval.texto, yytext);
                                  printf("Identificador: %s\n",yytext);
                                  return (IDENTIFICADOR);
                                  }

{SALTO_LINEA} linea++;

.
%%
