/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     CONSTANTE_REAL = 258,
     CONSTANTE_ENTERA = 259,
     CONSTANTE_CARACTER = 260,
     CONSTANTE_CADENA = 261,
     IDENTIFICADOR = 262,
     LIBRERIA = 263,
     ASIGNACION_IGUAL = 264,
     ASIGNACION_MULT = 265,
     ASIGNACION_DIV = 266,
     ASIGNACION_MOD = 267,
     ASIGNACION_MAS = 268,
     ASIGNACION_MENOS = 269,
     ASIGNACION_DESP_IZQ = 270,
     ASIGNACION_DESP_DER = 271,
     ASIGNACION_AMB = 272,
     ASIGNACION_XOR = 273,
     ASIGNACION_OR = 274,
     DEFINE = 275,
     INCLUDE = 276,
     IF = 277,
     ELSE = 278,
     SWITCH = 279,
     CASE = 280,
     DEFAULT = 281,
     DO = 282,
     WHILE = 283,
     FOR = 284,
     GOTO = 285,
     CONTINUE = 286,
     BREAK = 287,
     RETURN = 288,
     INT = 289,
     SHORT = 290,
     LONG = 291,
     DOUBLE = 292,
     FLOAT = 293,
     CHAR = 294,
     VOID = 295,
     UNSIGNED = 296,
     SIGNED = 297,
     EXTERN = 298,
     STATIC = 299,
     AUTO = 300,
     REGISTER = 301,
     OPERADOR_SIZEOF = 302,
     TYPEDEF = 303,
     SOSTENIDO = 304,
     STRUCT = 305,
     UNION = 306,
     INTERROGANTE = 307,
     COMA = 308,
     PUNTO_Y_COMA = 309,
     DOS_PUNTOS = 310,
     GUSANITO = 311,
     NEGACION_LOG = 312,
     PARIZQ = 313,
     PARDER = 314,
     CORCHETE_IZQ = 315,
     CORCHETE_DER = 316,
     LLAVE_IZQ = 317,
     LLAVE_DER = 318,
     PUNTO = 319,
     MEMORIA_FLECHA = 320,
     ARITMETICO_MENOSMENOS = 321,
     ARITMETICO_MASMAS = 322,
     OR_LOGICO = 323,
     AND_LOGICO = 324,
     RELACIONAL_DISTINTO = 325,
     RELACIONAL_IGUAL = 326,
     RELACIONAL_MENOR = 327,
     RELACIONAL_MAYOR = 328,
     RELACIONAL_MAYOR_IGUAL = 329,
     RELACIONAL_MENOR_IGUAL = 330,
     OR_BINARIO = 331,
     XOR_BINARIO = 332,
     AND_BINARIO = 333,
     DESP_DER = 334,
     DESP_IZQ = 335,
     SIGNO_MENOS = 336,
     SIGNO_MAS = 337,
     SIGNO_MOD = 338,
     SIGNO_DIV = 339,
     SIGNO_POR = 340,
     NEGATIVO = 341
   };
#endif
/* Tokens.  */
#define CONSTANTE_REAL 258
#define CONSTANTE_ENTERA 259
#define CONSTANTE_CARACTER 260
#define CONSTANTE_CADENA 261
#define IDENTIFICADOR 262
#define LIBRERIA 263
#define ASIGNACION_IGUAL 264
#define ASIGNACION_MULT 265
#define ASIGNACION_DIV 266
#define ASIGNACION_MOD 267
#define ASIGNACION_MAS 268
#define ASIGNACION_MENOS 269
#define ASIGNACION_DESP_IZQ 270
#define ASIGNACION_DESP_DER 271
#define ASIGNACION_AMB 272
#define ASIGNACION_XOR 273
#define ASIGNACION_OR 274
#define DEFINE 275
#define INCLUDE 276
#define IF 277
#define ELSE 278
#define SWITCH 279
#define CASE 280
#define DEFAULT 281
#define DO 282
#define WHILE 283
#define FOR 284
#define GOTO 285
#define CONTINUE 286
#define BREAK 287
#define RETURN 288
#define INT 289
#define SHORT 290
#define LONG 291
#define DOUBLE 292
#define FLOAT 293
#define CHAR 294
#define VOID 295
#define UNSIGNED 296
#define SIGNED 297
#define EXTERN 298
#define STATIC 299
#define AUTO 300
#define REGISTER 301
#define OPERADOR_SIZEOF 302
#define TYPEDEF 303
#define SOSTENIDO 304
#define STRUCT 305
#define UNION 306
#define INTERROGANTE 307
#define COMA 308
#define PUNTO_Y_COMA 309
#define DOS_PUNTOS 310
#define GUSANITO 311
#define NEGACION_LOG 312
#define PARIZQ 313
#define PARDER 314
#define CORCHETE_IZQ 315
#define CORCHETE_DER 316
#define LLAVE_IZQ 317
#define LLAVE_DER 318
#define PUNTO 319
#define MEMORIA_FLECHA 320
#define ARITMETICO_MENOSMENOS 321
#define ARITMETICO_MASMAS 322
#define OR_LOGICO 323
#define AND_LOGICO 324
#define RELACIONAL_DISTINTO 325
#define RELACIONAL_IGUAL 326
#define RELACIONAL_MENOR 327
#define RELACIONAL_MAYOR 328
#define RELACIONAL_MAYOR_IGUAL 329
#define RELACIONAL_MENOR_IGUAL 330
#define OR_BINARIO 331
#define XOR_BINARIO 332
#define AND_BINARIO 333
#define DESP_DER 334
#define DESP_IZQ 335
#define SIGNO_MENOS 336
#define SIGNO_MAS 337
#define SIGNO_MOD 338
#define SIGNO_DIV 339
#define SIGNO_POR 340
#define NEGATIVO 341




/* Copy the first part of user declarations.  */
#line 1 "c--.y"
 /* Codigo C */
#include <stdio.h>
#include <math.h>
extern FILE *yyin;


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 8 "c--.y"
{
  int valor_entero;
  double valor_real;
  char * texto;
  char caracter;
}
/* Line 193 of yacc.c.  */
#line 281 "c--.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 294 "c--.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   485

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  87
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  61
/* YYNRULES -- Number of rules.  */
#define YYNRULES  172
/* YYNRULES -- Number of states.  */
#define YYNSTATES  295

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   341

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     7,     8,    11,    13,    15,    17,
      20,    25,    29,    34,    36,    38,    40,    42,    46,    51,
      56,    58,    61,    62,    64,    68,    70,    74,    78,    79,
      83,    86,    88,    92,    94,    98,   101,   104,   109,   115,
     118,   122,   126,   128,   131,   133,   135,   136,   139,   141,
     143,   145,   147,   151,   155,   157,   159,   161,   163,   165,
     166,   168,   170,   171,   173,   175,   178,   179,   182,   184,
     186,   188,   190,   192,   194,   196,   201,   202,   205,   207,
     210,   213,   216,   220,   222,   224,   226,   228,   230,   232,
     234,   236,   238,   240,   242,   248,   256,   264,   269,   273,
     279,   287,   297,   298,   300,   304,   308,   311,   314,   319,
     323,   326,   328,   334,   338,   340,   344,   346,   350,   354,
     356,   360,   364,   368,   372,   374,   378,   380,   384,   386,
     390,   392,   396,   400,   402,   406,   410,   412,   416,   420,
     424,   426,   428,   431,   436,   439,   442,   445,   447,   449,
     451,   454,   457,   459,   461,   463,   465,   467,   469,   471,
     476,   481,   482,   484,   488,   490,   495,   499,   503,   505,
     507,   509,   511
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
      88,     0,    -1,    -1,    89,    90,    -1,    -1,    89,    90,
      -1,    91,    -1,    94,    -1,    92,    -1,     7,   116,    -1,
     102,   114,     7,   116,    -1,    49,    21,     8,    -1,    49,
      20,     7,    93,    -1,     3,    -1,     4,    -1,     6,    -1,
       5,    -1,   102,    96,    54,    -1,   102,    96,    49,    54,
      -1,    48,   102,    95,    54,    -1,     7,    -1,     7,    95,
      -1,    -1,    97,    -1,    97,    53,    96,    -1,    98,    -1,
      98,     9,   100,    -1,   114,     7,    99,    -1,    -1,    60,
     129,    61,    -1,    60,    61,    -1,   129,    -1,    62,   101,
      63,    -1,   100,    -1,   100,    53,   101,    -1,   107,   109,
      -1,   107,   103,    -1,   106,    62,   105,    63,    -1,   106,
       7,    62,   105,    63,    -1,   106,     7,    -1,   109,    96,
      54,    -1,   103,    96,    54,    -1,   104,    -1,   104,   105,
      -1,    50,    -1,    51,    -1,    -1,   108,   107,    -1,    43,
      -1,    44,    -1,    45,    -1,    46,    -1,   111,   112,   110,
      -1,    60,     7,    61,    -1,    40,    -1,    39,    -1,    34,
      -1,    38,    -1,    37,    -1,    -1,    42,    -1,    41,    -1,
      -1,    35,    -1,    36,    -1,   109,   114,    -1,    -1,    85,
     114,    -1,   116,    -1,   119,    -1,   122,    -1,   124,    -1,
     126,    -1,   127,    -1,   128,    -1,    62,   117,   118,    63,
      -1,    -1,    94,   117,    -1,   115,    -1,   115,   118,    -1,
     144,    54,    -1,   120,    54,    -1,   146,   121,   129,    -1,
       9,    -1,    10,    -1,    11,    -1,    12,    -1,    13,    -1,
      14,    -1,    15,    -1,    16,    -1,    17,    -1,    18,    -1,
      19,    -1,    22,    58,   129,    59,   115,    -1,    22,    58,
     129,    59,   115,    23,   115,    -1,    24,    58,   129,    59,
      62,   123,    63,    -1,    25,   129,    55,   115,    -1,    26,
      55,   115,    -1,    28,    58,   129,    59,   115,    -1,    27,
     115,    28,    58,   129,    59,    54,    -1,    29,    58,   125,
      54,   129,    54,   129,    59,   115,    -1,    -1,   120,    -1,
     120,    53,   125,    -1,    30,     7,    54,    -1,    31,    54,
      -1,    32,    54,    -1,     7,    55,   115,    54,    -1,    33,
     129,    54,    -1,    33,    54,    -1,   130,    -1,   130,    52,
     129,    55,   129,    -1,   130,    68,   131,    -1,   131,    -1,
     131,    69,   132,    -1,   132,    -1,   132,    71,   133,    -1,
     132,    70,   133,    -1,   133,    -1,   133,    72,   134,    -1,
     133,    73,   134,    -1,   133,    75,   134,    -1,   133,    74,
     134,    -1,   134,    -1,   134,    76,   135,    -1,   135,    -1,
     135,    77,   136,    -1,   136,    -1,   136,    78,   137,    -1,
     137,    -1,   137,    80,   138,    -1,   137,    79,   138,    -1,
     138,    -1,   138,    82,   139,    -1,   138,    81,   139,    -1,
     139,    -1,   139,    85,   140,    -1,   139,    84,   140,    -1,
     139,    83,   140,    -1,   140,    -1,   141,    -1,    47,   140,
      -1,    47,    58,   113,    59,    -1,    67,   140,    -1,    66,
     140,    -1,   142,   143,    -1,   147,    -1,   144,    -1,   146,
      -1,   141,    67,    -1,   141,    66,    -1,    78,    -1,    85,
      -1,    82,    -1,    81,    -1,    56,    -1,    57,    -1,   140,
      -1,    58,   113,    59,   143,    -1,     7,    58,   145,    59,
      -1,    -1,   129,    -1,   129,    53,   145,    -1,     7,    -1,
     146,    60,   129,    61,    -1,   146,    64,     7,    -1,   146,
      65,     7,    -1,     3,    -1,     4,    -1,     5,    -1,     6,
      -1,    58,   129,    59,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    86,    86,    87,    90,    91,    94,    95,    96,    99,
     100,   103,   104,   107,   108,   109,   110,   113,   114,   115,
     118,   119,   123,   124,   125,   128,   129,   133,   136,   137,
     138,   141,   142,   145,   146,   149,   150,   153,   154,   155,
     158,   159,   162,   163,   166,   166,   169,   170,   173,   174,
     175,   176,   179,   180,   183,   184,   185,   186,   187,   190,
     191,   192,   195,   196,   197,   200,   203,   204,   209,   210,
     211,   212,   213,   214,   215,   218,   221,   222,   225,   226,
     229,   230,   233,   236,   237,   238,   239,   240,   241,   242,
     243,   244,   245,   246,   249,   250,   251,   256,   257,   260,
     261,   262,   265,   266,   267,   270,   271,   272,   275,   278,
     279,   289,   290,   293,   294,   297,   298,   301,   302,   303,
     306,   307,   308,   309,   310,   313,   314,   317,   318,   321,
     322,   325,   326,   327,   330,   331,   332,   335,   336,   337,
     338,   341,   342,   343,   344,   345,   346,   349,   350,   351,
     352,   353,   356,   357,   358,   359,   360,   361,   364,   365,
     368,   371,   372,   373,   376,   377,   378,   379,   382,   383,
     384,   385,   386
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "CONSTANTE_REAL", "CONSTANTE_ENTERA",
  "CONSTANTE_CARACTER", "CONSTANTE_CADENA", "IDENTIFICADOR", "LIBRERIA",
  "ASIGNACION_IGUAL", "ASIGNACION_MULT", "ASIGNACION_DIV",
  "ASIGNACION_MOD", "ASIGNACION_MAS", "ASIGNACION_MENOS",
  "ASIGNACION_DESP_IZQ", "ASIGNACION_DESP_DER", "ASIGNACION_AMB",
  "ASIGNACION_XOR", "ASIGNACION_OR", "DEFINE", "INCLUDE", "IF", "ELSE",
  "SWITCH", "CASE", "DEFAULT", "DO", "WHILE", "FOR", "GOTO", "CONTINUE",
  "BREAK", "RETURN", "INT", "SHORT", "LONG", "DOUBLE", "FLOAT", "CHAR",
  "VOID", "UNSIGNED", "SIGNED", "EXTERN", "STATIC", "AUTO", "REGISTER",
  "OPERADOR_SIZEOF", "TYPEDEF", "SOSTENIDO", "STRUCT", "UNION",
  "INTERROGANTE", "COMA", "PUNTO_Y_COMA", "DOS_PUNTOS", "GUSANITO",
  "NEGACION_LOG", "PARIZQ", "PARDER", "CORCHETE_IZQ", "CORCHETE_DER",
  "LLAVE_IZQ", "LLAVE_DER", "PUNTO", "MEMORIA_FLECHA",
  "ARITMETICO_MENOSMENOS", "ARITMETICO_MASMAS", "OR_LOGICO", "AND_LOGICO",
  "RELACIONAL_DISTINTO", "RELACIONAL_IGUAL", "RELACIONAL_MENOR",
  "RELACIONAL_MAYOR", "RELACIONAL_MAYOR_IGUAL", "RELACIONAL_MENOR_IGUAL",
  "OR_BINARIO", "XOR_BINARIO", "AND_BINARIO", "DESP_DER", "DESP_IZQ",
  "SIGNO_MENOS", "SIGNO_MAS", "SIGNO_MOD", "SIGNO_DIV", "SIGNO_POR",
  "NEGATIVO", "$accept", "programa", "bloques", "bloque",
  "definicion_funcion", "macros", "constante", "declaracion",
  "lista_identificador", "lista_nombres", "nombre", "dato",
  "exp_corchetes", "elementos", "lista_elementos", "declaracion_tipo",
  "definicion_struct_union", "declaracion_struct",
  "lista_declaracion_struct", "struct_union", "lista_almacenamiento",
  "almacenamiento", "tipo_basico_modificado", "tipo_basico", "signo",
  "longitud", "nombre_tipo", "lista_asteriscos", "instruccion",
  "bloque_instrucciones", "lista_declaraciones", "lista_instrucciones",
  "instruccion_expresion", "asignacion", "operador_asignacion",
  "instruccion_bifurcacion", "instruccion_caso", "instruccion_bucle",
  "lista_asignaciones", "instruccion_salto", "instruccion_destino_salto",
  "instruccion_retorno", "expresion", "expresion_logica", "expresion_1",
  "expresion_2", "expresion_3", "expresion_4", "expresion_5",
  "expresion_6", "expresion_7", "expresion_8", "expresion_9",
  "expresion_prefija", "expresion_postfija", "operador_unario",
  "expresion_cast", "expresion_funcional", "lista_expresiones",
  "expresion_indexada", "expresion_constante", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    87,    88,    88,    89,    89,    90,    90,    90,    91,
      91,    92,    92,    93,    93,    93,    93,    94,    94,    94,
      95,    95,    96,    96,    96,    97,    97,    98,    99,    99,
      99,   100,   100,   101,   101,   102,   102,   103,   103,   103,
     104,   104,   105,   105,   106,   106,   107,   107,   108,   108,
     108,   108,   109,   109,   110,   110,   110,   110,   110,   111,
     111,   111,   112,   112,   112,   113,   114,   114,   115,   115,
     115,   115,   115,   115,   115,   116,   117,   117,   118,   118,
     119,   119,   120,   121,   121,   121,   121,   121,   121,   121,
     121,   121,   121,   121,   122,   122,   122,   123,   123,   124,
     124,   124,   125,   125,   125,   126,   126,   126,   127,   128,
     128,   129,   129,   130,   130,   131,   131,   132,   132,   132,
     133,   133,   133,   133,   133,   134,   134,   135,   135,   136,
     136,   137,   137,   137,   138,   138,   138,   139,   139,   139,
     139,   140,   140,   140,   140,   140,   140,   141,   141,   141,
     141,   141,   142,   142,   142,   142,   142,   142,   143,   143,
     144,   145,   145,   145,   146,   146,   146,   146,   147,   147,
     147,   147,   147
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     2,     0,     2,     1,     1,     1,     2,
       4,     3,     4,     1,     1,     1,     1,     3,     4,     4,
       1,     2,     0,     1,     3,     1,     3,     3,     0,     3,
       2,     1,     3,     1,     3,     2,     2,     4,     5,     2,
       3,     3,     1,     2,     1,     1,     0,     2,     1,     1,
       1,     1,     3,     3,     1,     1,     1,     1,     1,     0,
       1,     1,     0,     1,     1,     2,     0,     2,     1,     1,
       1,     1,     1,     1,     1,     4,     0,     2,     1,     2,
       2,     2,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     5,     7,     7,     4,     3,     5,
       7,     9,     0,     1,     3,     3,     2,     2,     4,     3,
       2,     1,     5,     3,     1,     3,     1,     3,     3,     1,
       3,     3,     3,     3,     1,     3,     1,     3,     1,     3,
       1,     3,     3,     1,     3,     3,     1,     3,     3,     3,
       1,     1,     2,     4,     2,     2,     2,     1,     1,     1,
       2,     2,     1,     1,     1,     1,     1,     1,     1,     4,
       4,     0,     1,     3,     1,     4,     3,     3,     1,     1,
       1,     1,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       4,     0,    46,     1,     0,    48,    49,    50,    51,    46,
       0,     5,     6,     8,     7,    22,    59,    46,    46,     9,
       0,     0,     0,    66,     0,    23,    25,     0,    61,    60,
      44,    45,     0,    36,     0,    35,    62,    47,    46,    22,
       0,    20,     0,     0,    11,    67,     0,    17,    22,     0,
      28,     0,    39,    59,    63,    64,     0,    77,     0,   164,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    78,
      68,     0,    69,     0,    70,    71,    72,    73,    74,     0,
       0,    21,    19,    13,    14,    16,    15,    12,    18,    24,
     168,   169,   170,   171,   164,     0,   156,   157,     0,     0,
       0,     0,   152,   155,   154,   153,    26,    31,   111,   114,
     116,   119,   124,   126,   128,   130,   133,   136,   140,   141,
       0,   148,   149,   147,     0,    27,    10,    53,    59,    22,
      59,     0,    22,    56,    58,    57,    55,    54,    52,    28,
       0,   161,     0,     0,     0,     0,   102,     0,   106,   107,
     110,     0,    79,    75,    81,    80,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,     0,     0,     0,
       0,    59,   142,     0,    33,     0,   145,   144,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   151,   150,    59,
     158,   146,    30,     0,     0,     0,    43,    37,     0,     0,
     162,     0,     0,     0,     0,     0,   164,   103,     0,   105,
     109,     0,   166,   167,    82,    66,     0,   172,     0,    32,
       0,   113,   115,   118,   117,   120,   121,   123,   122,   125,
     127,   129,   132,   131,   135,   134,   139,   138,   137,     0,
      29,    38,    41,    40,   108,   161,   160,     0,     0,     0,
       0,   102,     0,   165,    65,   143,    34,     0,     0,   163,
      94,     0,     0,    99,   104,     0,   112,   159,     0,     0,
       0,     0,     0,     0,    95,     0,     0,    96,   100,     0,
       0,    98,     0,    97,   101
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     2,    11,    12,    13,    87,    38,    42,    24,
      25,    26,   125,   174,   175,    39,   129,   130,   131,    34,
      16,    17,   132,   138,    36,    56,   226,    58,    69,    70,
      40,    71,    72,    73,   170,    74,   281,    75,   218,    76,
      77,    78,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   201,   121,   211,   122,
     123
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -143
static const yytype_int16 yypact[] =
{
      20,    31,    14,  -143,    28,  -143,  -143,  -143,  -143,   109,
       5,    61,  -143,  -143,  -143,    -1,    32,   109,   423,  -143,
      92,    94,   102,    33,   -31,    70,   137,   143,  -143,  -143,
    -143,  -143,   167,  -143,     2,  -143,    10,  -143,   423,    -1,
       8,    92,   121,   131,  -143,  -143,   125,  -143,    -1,   230,
       9,   122,   118,    32,  -143,  -143,    38,  -143,   175,   -39,
     127,   128,     8,   129,   130,   182,   136,   138,   260,     8,
    -143,   132,  -143,   139,  -143,  -143,  -143,  -143,  -143,   140,
     373,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,   133,    46,  -143,  -143,   266,   230,
     266,   266,  -143,  -143,  -143,  -143,  -143,  -143,   -24,   135,
     -16,    87,   120,   124,   134,    37,    44,   -18,  -143,    74,
     361,  -143,   -17,  -143,   296,  -143,  -143,  -143,    32,    -1,
      79,   142,    -1,  -143,  -143,  -143,  -143,  -143,  -143,   147,
       8,   266,   266,   266,   170,   266,   195,   154,  -143,  -143,
    -143,   155,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,   266,   203,   207,
     266,    91,  -143,   156,   163,   161,  -143,  -143,   266,   266,
     266,   266,   266,   266,   266,   266,   266,   266,   266,   266,
     266,   266,   266,   266,   266,   266,   266,  -143,  -143,    91,
    -143,  -143,  -143,   158,   165,   177,  -143,  -143,   185,   186,
     176,   183,   188,   189,   187,   190,  -143,   191,   199,  -143,
    -143,   180,  -143,  -143,  -143,    33,   197,  -143,   230,  -143,
     204,   135,   -16,    87,    87,   120,   120,   120,   120,   124,
     134,    37,    44,    44,   -18,   -18,  -143,  -143,  -143,   201,
    -143,  -143,  -143,  -143,  -143,   266,  -143,     8,   192,   266,
       8,   195,   266,  -143,  -143,  -143,  -143,   266,   361,  -143,
     235,   119,   202,  -143,  -143,   208,  -143,  -143,     8,   266,
     213,   211,   221,   266,  -143,   223,     8,  -143,  -143,   217,
       8,  -143,     8,  -143,  -143
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -143,  -143,  -143,  -143,  -143,  -143,  -143,   277,   239,   -41,
    -143,  -143,  -143,   232,    54,    15,   267,  -143,   -49,  -143,
     268,  -143,   -15,  -143,  -143,  -143,    85,   -12,   -60,     6,
     251,   222,  -143,  -142,  -143,  -143,  -143,  -143,    29,  -143,
    -143,  -143,   -56,  -143,   114,   115,   -14,   -20,   107,   110,
     116,   -13,   -22,   -87,  -143,  -143,    36,   -35,    51,   -40,
    -143
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -77
static const yytype_int16 yytable[] =
{
      80,    35,   144,    27,   217,    79,   -66,    89,   172,    52,
      19,    45,   151,   176,   177,    59,   140,    15,    46,   141,
      -2,     4,    80,    47,    20,    21,    22,    79,   178,    80,
      60,     3,    61,   200,    79,    62,    63,    64,    65,    66,
      67,    68,   173,   167,   179,    54,    55,   168,   169,    90,
      91,    92,    93,    94,   181,   182,   126,     5,     6,     7,
       8,    -3,     9,    10,    53,   194,   195,   196,   203,   124,
      18,    18,   133,    28,    29,   134,   135,   136,   137,   204,
     209,   206,    30,    31,    23,   210,   212,   213,   205,   215,
      18,   208,    32,    95,    90,    91,    92,    93,    94,    41,
      80,    43,    96,    97,   171,    79,    80,   246,   247,   248,
      44,   221,   100,   101,   224,   173,   190,   191,    23,   217,
      28,    29,   230,    48,   102,   192,   193,   103,   104,    30,
      31,   105,    28,    29,    83,    84,    85,    86,    95,    32,
     197,   198,   -42,   173,   279,   280,    49,    96,    97,    98,
      50,    32,     5,     6,     7,     8,   225,   100,   101,   183,
     184,   185,   186,   235,   236,   237,   238,   233,   234,   102,
     244,   245,   103,   104,    51,    82,   105,   242,   243,    88,
     128,   200,   139,   127,   225,   142,   143,   145,   146,   147,
     148,   141,   149,   154,   155,   153,   187,   270,   214,   210,
     273,   188,   216,   272,   180,   207,   275,   124,   219,   220,
     222,   276,   189,   264,   223,   227,   228,    80,   284,   250,
      80,    80,    79,   285,   229,    79,   291,   289,   251,   255,
     293,   252,   294,    90,    91,    92,    93,    94,    80,   253,
     254,   263,   256,    79,   261,   259,    80,   257,   258,   260,
      80,    79,    80,   262,   271,    79,   265,    79,   278,   267,
     268,   282,   283,    90,    91,    92,    93,    94,   286,    90,
      91,    92,    93,    94,   287,   288,   292,    95,   290,    14,
      81,   106,   266,    33,   249,    37,    96,    97,    98,    57,
     274,   152,    99,   231,   239,   232,   100,   101,   240,    90,
      91,    92,    93,    94,   277,   241,   269,    95,   102,     0,
       0,   103,   104,    95,   150,   105,    96,    97,    98,     0,
       0,     0,    96,    97,    98,     0,   100,   101,     0,     0,
       0,     0,   100,   101,     0,     0,     0,     0,   102,     0,
       0,   103,   104,    95,   102,   105,     0,   103,   104,     0,
       0,   105,    96,    97,    98,     0,     0,   202,     0,     0,
       0,     0,   100,   101,    90,    91,    92,    93,    94,     0,
       0,     0,     0,     0,   102,     0,     0,   103,   104,     0,
       0,   105,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    95,     0,
       0,     0,     0,     0,     0,     0,     0,    96,    97,   199,
       0,     0,     0,     0,     0,     0,     0,   100,   101,     0,
     -76,     0,     0,   167,     0,     0,     0,   168,   169,   102,
       0,     0,   103,   104,     0,   -76,   105,   -76,     0,     0,
     -76,   -76,   -76,   -76,   -76,   -76,   -76,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     5,     6,     7,     8,
       0,     9,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   -76
};

static const yytype_int16 yycheck[] =
{
      40,    16,    62,    15,   146,    40,     7,    48,    95,     7,
       4,    23,    68,   100,   101,     7,    55,     2,    49,    58,
       0,     7,    62,    54,     9,    20,    21,    62,    52,    69,
      22,     0,    24,   120,    69,    27,    28,    29,    30,    31,
      32,    33,    98,    60,    68,    35,    36,    64,    65,     3,
       4,     5,     6,     7,    70,    71,    50,    43,    44,    45,
      46,     0,    48,    49,    62,    83,    84,    85,   124,    60,
      62,    62,    34,    41,    42,    37,    38,    39,    40,   128,
     140,   130,    50,    51,    85,   141,   142,   143,   129,   145,
      62,   132,    60,    47,     3,     4,     5,     6,     7,     7,
     140,     7,    56,    57,    58,   140,   146,   194,   195,   196,
       8,   167,    66,    67,   170,   171,    79,    80,    85,   261,
      41,    42,   178,    53,    78,    81,    82,    81,    82,    50,
      51,    85,    41,    42,     3,     4,     5,     6,    47,    60,
      66,    67,    63,   199,    25,    26,     9,    56,    57,    58,
       7,    60,    43,    44,    45,    46,   171,    66,    67,    72,
      73,    74,    75,   183,   184,   185,   186,   181,   182,    78,
     192,   193,    81,    82,     7,    54,    85,   190,   191,    54,
      62,   268,     7,    61,   199,    58,    58,    58,    58,     7,
      54,    58,    54,    54,    54,    63,    76,   257,    28,   255,
     260,    77,     7,   259,    69,    63,   262,    60,    54,    54,
       7,   267,    78,   225,     7,    59,    53,   257,   278,    61,
     260,   261,   257,   279,    63,   260,   286,   283,    63,    53,
     290,    54,   292,     3,     4,     5,     6,     7,   278,    54,
      54,    61,    59,   278,    53,    58,   286,    59,    59,    59,
     290,   286,   292,    54,    62,   290,    59,   292,    23,    55,
      59,    59,    54,     3,     4,     5,     6,     7,    55,     3,
       4,     5,     6,     7,    63,    54,    59,    47,    55,     2,
      41,    49,   228,    16,   199,    17,    56,    57,    58,    38,
     261,    69,    62,   179,   187,   180,    66,    67,   188,     3,
       4,     5,     6,     7,   268,   189,   255,    47,    78,    -1,
      -1,    81,    82,    47,    54,    85,    56,    57,    58,    -1,
      -1,    -1,    56,    57,    58,    -1,    66,    67,    -1,    -1,
      -1,    -1,    66,    67,    -1,    -1,    -1,    -1,    78,    -1,
      -1,    81,    82,    47,    78,    85,    -1,    81,    82,    -1,
      -1,    85,    56,    57,    58,    -1,    -1,    61,    -1,    -1,
      -1,    -1,    66,    67,     3,     4,     5,     6,     7,    -1,
      -1,    -1,    -1,    -1,    78,    -1,    -1,    81,    82,    -1,
      -1,    85,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    47,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    56,    57,    58,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    66,    67,    -1,
       7,    -1,    -1,    60,    -1,    -1,    -1,    64,    65,    78,
      -1,    -1,    81,    82,    -1,    22,    85,    24,    -1,    -1,
      27,    28,    29,    30,    31,    32,    33,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    43,    44,    45,    46,
      -1,    48,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    62
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    88,    89,     0,     7,    43,    44,    45,    46,    48,
      49,    90,    91,    92,    94,   102,   107,   108,    62,   116,
     102,    20,    21,    85,    96,    97,    98,   114,    41,    42,
      50,    51,    60,   103,   106,   109,   111,   107,    94,   102,
     117,     7,    95,     7,     8,   114,    49,    54,    53,     9,
       7,     7,     7,    62,    35,    36,   112,   117,   114,     7,
      22,    24,    27,    28,    29,    30,    31,    32,    33,   115,
     116,   118,   119,   120,   122,   124,   126,   127,   128,   144,
     146,    95,    54,     3,     4,     5,     6,    93,    54,    96,
       3,     4,     5,     6,     7,    47,    56,    57,    58,    62,
      66,    67,    78,    81,    82,    85,   100,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   144,   146,   147,    60,    99,   116,    61,    62,   103,
     104,   105,   109,    34,    37,    38,    39,    40,   110,     7,
      55,    58,    58,    58,   115,    58,    58,     7,    54,    54,
      54,   129,   118,    63,    54,    54,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    60,    64,    65,
     121,    58,   140,   129,   100,   101,   140,   140,    52,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    66,    67,    58,
     140,   143,    61,   129,   105,    96,   105,    63,    96,   115,
     129,   145,   129,   129,    28,   129,     7,   120,   125,    54,
      54,   129,     7,     7,   129,   109,   113,    59,    53,    63,
     129,   131,   132,   133,   133,   134,   134,   134,   134,   135,
     136,   137,   138,   138,   139,   139,   140,   140,   140,   113,
      61,    63,    54,    54,    54,    53,    59,    59,    59,    58,
      59,    53,    54,    61,   114,    59,   101,    55,    59,   145,
     115,    62,   129,   115,   125,   129,   129,   143,    23,    25,
      26,   123,    59,    54,   115,   129,    55,    63,    54,   129,
      55,   115,    59,   115,   115
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 6:
#line 94 "c--.y"
    { printf("bloque ::= definicion_funcion\n"); ;}
    break;

  case 7:
#line 95 "c--.y"
    { printf("bloque ::= declaracion\n"); ;}
    break;

  case 8:
#line 96 "c--.y"
    { printf("bloque ::= macros\n"); ;}
    break;

  case 9:
#line 99 "c--.y"
    { printf("definicion_funcion ::= bloque_instrucciones\n"); ;}
    break;

  case 10:
#line 100 "c--.y"
    { printf("definicion_funcion ::= declaracion_tipo lista_asteriscos IDENTIFICADOR bloque_instrucciones\n"); ;}
    break;

  case 13:
#line 107 "c--.y"
    { printf("constante ::= constante_real\n"); ;}
    break;

  case 14:
#line 108 "c--.y"
    { printf("constante ::= constante_entera\n"); ;}
    break;

  case 15:
#line 109 "c--.y"
    { printf("constante ::= constante_cadena\n"); ;}
    break;

  case 16:
#line 110 "c--.y"
    { printf("constante ::= constante_caracter\n"); ;}
    break;

  case 17:
#line 113 "c--.y"
    { printf("declaracion ::= declaracion_tipo lista_nombres ;\n"); ;}
    break;

  case 18:
#line 114 "c--.y"
    { printf("declaracion ::= declaracion_tipo lista_nombres # ;\n"); ;}
    break;

  case 19:
#line 115 "c--.y"
    { printf("typedef declaracion_tipo lista_identificador ;\n"); ;}
    break;

  case 27:
#line 133 "c--.y"
    { printf("lista_asteriscos IDENTIFICADOR exp_corchetes\n"); ;}
    break;

  case 29:
#line 137 "c--.y"
    { printf("exp_corchetes ::= [expresion]\n"); ;}
    break;

  case 30:
#line 138 "c--.y"
    { printf("exp_corchetes ::= []\n"); ;}
    break;

  case 31:
#line 141 "c--.y"
    { printf("elementos ::= expresion\n"); ;}
    break;

  case 32:
#line 142 "c--.y"
    { printf("elementos ::= {lista_elementos}\n"); ;}
    break;

  case 35:
#line 149 "c--.y"
    { printf("declaracion_tipo ::= lista_almacenamiento tipo_basico_modificado\n"); ;}
    break;

  case 36:
#line 150 "c--.y"
    { printf("declaracion_tipo ::= lista_almacenamiento definicion_struct_union\n"); ;}
    break;

  case 37:
#line 153 "c--.y"
    { printf("definicion_struct_union ::= struct_union {lista_declaracion_struct}\n"); ;}
    break;

  case 38:
#line 154 "c--.y"
    { printf("definicion_struct_union ::= struct_union IDENTIFICADOR {lista_declaracion_struct}\n"); ;}
    break;

  case 39:
#line 155 "c--.y"
    { printf("definicion_struct_union ::= struct_union IDENTIFICADOR\n"); ;}
    break;

  case 40:
#line 158 "c--.y"
    { printf("declaracion_struct ::= tipo_basico_modificado lista_nombres ;\n"); ;}
    break;

  case 41:
#line 159 "c--.y"
    { printf("declaracion_struct ::= definicion_struct_union lista_nombres ;\n"); ;}
    break;

  case 44:
#line 166 "c--.y"
    { printf("struct_union ::= struct\n"); ;}
    break;

  case 45:
#line 166 "c--.y"
    { printf("struct_union ::= union\n"); ;}
    break;

  case 48:
#line 173 "c--.y"
    { printf("almacenamiento ::= extern\n"); ;}
    break;

  case 49:
#line 174 "c--.y"
    { printf("almacenamiento ::= static\n"); ;}
    break;

  case 50:
#line 175 "c--.y"
    { printf("almacenamiento ::= auto\n"); ;}
    break;

  case 51:
#line 176 "c--.y"
    { printf("almacenamiento ::= register\n"); ;}
    break;

  case 54:
#line 183 "c--.y"
    { printf("tipo_basico ::= void\n"); ;}
    break;

  case 55:
#line 184 "c--.y"
    { printf("tipo_basico ::= char\n"); ;}
    break;

  case 56:
#line 185 "c--.y"
    { printf("tipo_basico ::= int\n"); ;}
    break;

  case 57:
#line 186 "c--.y"
    { printf("tipo_basico ::= float\n"); ;}
    break;

  case 58:
#line 187 "c--.y"
    { printf("tipo_basico ::= double\n"); ;}
    break;

  case 60:
#line 191 "c--.y"
    { printf("signo ::= signed\n"); ;}
    break;

  case 61:
#line 192 "c--.y"
    { printf("signo ::= unsigned\n"); ;}
    break;

  case 63:
#line 196 "c--.y"
    { printf("longitud ::= short\n"); ;}
    break;

  case 64:
#line 197 "c--.y"
    { printf("longitud ::= long\n"); ;}
    break;

  case 65:
#line 200 "c--.y"
    { printf("nombre_tipo ::= tipo_basico_modificado lista_asteriscos\n"); ;}
    break;

  case 68:
#line 209 "c--.y"
    { printf("instruccion ::= bloque_instrucciones\n"); ;}
    break;

  case 69:
#line 210 "c--.y"
    { printf("instruccion ::= instruccion_expresion\n"); ;}
    break;

  case 70:
#line 211 "c--.y"
    { printf("instruccion ::= instruccion_bifurcacion\n"); ;}
    break;

  case 71:
#line 212 "c--.y"
    { printf("instruccion ::= instruccion_bucle\n"); ;}
    break;

  case 72:
#line 213 "c--.y"
    { printf("instruccion ::= instruccion_salto\n"); ;}
    break;

  case 73:
#line 214 "c--.y"
    { printf("instruccion ::= instruccion_destino_salto\n"); ;}
    break;

  case 74:
#line 215 "c--.y"
    { printf("instruccion ::= instruccion_retorno\n"); ;}
    break;

  case 75:
#line 218 "c--.y"
    { printf("bloque_instrucciones ::= { lista_declaraciones lista_instrucciones }\n"); ;}
    break;

  case 77:
#line 222 "c--.y"
    { printf("lista_declaraciones ::= declaracion lista_declaraciones\n"); ;}
    break;

  case 78:
#line 225 "c--.y"
    { printf("lista_instrucciones ::= instruccion\n"); ;}
    break;

  case 79:
#line 226 "c--.y"
    { printf("lista_instrucciones ::= instruccion lista_instrucciones\n"); ;}
    break;

  case 80:
#line 229 "c--.y"
    { printf("instruccion_expresion ::= expresion_funcional;\n"); ;}
    break;

  case 81:
#line 230 "c--.y"
    { printf("instruccion_expresion ::= asignacion;\n"); ;}
    break;

  case 82:
#line 233 "c--.y"
    { printf("asignacion ::= expresion_indexada operador_asignacion expresion\n"); ;}
    break;

  case 94:
#line 249 "c--.y"
    { printf("instruccion_bifurcacion ::= if ( expresion ) instruccion\n"); ;}
    break;

  case 95:
#line 250 "c--.y"
    { printf("instruccion_bifurcacion ::= if ( expresion ) instruccion else instruccion\n"); ;}
    break;

  case 96:
#line 251 "c--.y"
    { printf("instruccion_bifurcacion ::= switch (expresion) {instruccion_caso}\n"); ;}
    break;

  case 97:
#line 256 "c--.y"
    { printf("instruccion_caso ::= case expresion : instruccion\n"); ;}
    break;

  case 98:
#line 257 "c--.y"
    { printf("instruccion_caso ::= default expresion : instruccion\n"); ;}
    break;

  case 99:
#line 260 "c--.y"
    { printf("instruccion_bucle ::=  while ( expresion ) instruccion\n"); ;}
    break;

  case 100:
#line 261 "c--.y"
    { printf("instruccion_bucle ::= do instruccion while ( expresion );\n"); ;}
    break;

  case 101:
#line 262 "c--.y"
    { printf("for(lista_asignaciones; expresion; expresion) instruccion\n"); ;}
    break;

  case 103:
#line 266 "c--.y"
    { printf("lista_asignaciones ::= asignacion\n"); ;}
    break;

  case 104:
#line 267 "c--.y"
    { printf("lista_asignaciones ::= asignacion , lista_asignaciones\n"); ;}
    break;

  case 105:
#line 270 "c--.y"
    { printf("instruccion_salto ::= GOTO INDENTIFICADOR ;\n"); ;}
    break;

  case 106:
#line 271 "c--.y"
    { printf("instruccion_salto ::=  continue ;\n"); ;}
    break;

  case 107:
#line 272 "c--.y"
    { printf("instruccion_salto ::=  break ;\n"); ;}
    break;

  case 108:
#line 275 "c--.y"
    { printf("IDENTIFICADOR:instruccion;\n"); ;}
    break;

  case 109:
#line 278 "c--.y"
    { printf("instruccion_retorno ::= return expresion ;\n"); ;}
    break;

  case 110:
#line 279 "c--.y"
    { printf("instruccion_retorno ::= return ;\n"); ;}
    break;

  case 111:
#line 289 "c--.y"
    { printf("expresion ::= expresion_logica \n"); ;}
    break;

  case 112:
#line 290 "c--.y"
    { printf("expresion ::= expresion_logica ? expresion : expresion \n"); ;}
    break;

  case 113:
#line 293 "c--.y"
    { printf("expresion_logica ::= expresion_logica || expresion_1 \n"); ;}
    break;

  case 114:
#line 294 "c--.y"
    { printf("expresion_logica ::= expresion_1 \n"); ;}
    break;

  case 115:
#line 297 "c--.y"
    { printf("expresion_1 ::= expresion_1 && expresion_2\n"); ;}
    break;

  case 116:
#line 298 "c--.y"
    { printf("expresion_1 ::= expresion_2\n"); ;}
    break;

  case 117:
#line 301 "c--.y"
    { printf("expresion_2 ::= expresion_2 == expresion_3\n"); ;}
    break;

  case 118:
#line 302 "c--.y"
    { printf("expresion_2 ::= expresion_2 != expresion_3\n"); ;}
    break;

  case 119:
#line 303 "c--.y"
    { printf("expresion_2 ::= expresion_3\n"); ;}
    break;

  case 120:
#line 306 "c--.y"
    { printf("expresion_3 ::= expresion_3 < expresion_4\n"); ;}
    break;

  case 121:
#line 307 "c--.y"
    { printf("expresion_3 ::= expresion_3 > expresion_4 \n"); ;}
    break;

  case 122:
#line 308 "c--.y"
    { printf("expresion_3 ::= expresion_3 <= expresion_4\n"); ;}
    break;

  case 123:
#line 309 "c--.y"
    { printf("expresion_3 ::= expresion_3 >= expresion_4\n"); ;}
    break;

  case 124:
#line 310 "c--.y"
    { printf("expresion_3 ::= expresion_4\n"); ;}
    break;

  case 125:
#line 313 "c--.y"
    { printf("expresion_4 ::= expresion_4 | expresion_5\n"); ;}
    break;

  case 126:
#line 314 "c--.y"
    { printf("expresion_4 ::= expresion_5\n"); ;}
    break;

  case 127:
#line 317 "c--.y"
    { printf("expresion_5 ::= expresion_5 ^ expresion_6\n"); ;}
    break;

  case 128:
#line 318 "c--.y"
    { printf("expresion_5 ::= expresion_6\n");;}
    break;

  case 129:
#line 321 "c--.y"
    { printf("expresion_6 ::= expresion_6 & expresion_7\n"); ;}
    break;

  case 130:
#line 322 "c--.y"
    { printf("expresion_6 ::= expresion_7\n"); ;}
    break;

  case 131:
#line 325 "c--.y"
    { printf("expresion_7 ::= expresion_7 << expresion_8\n"); ;}
    break;

  case 132:
#line 326 "c--.y"
    { printf("expresion_7 ::= expresion_7 >> expresion_8\n"); ;}
    break;

  case 133:
#line 327 "c--.y"
    { printf("expresion_7 ::= expresion_8\n"); ;}
    break;

  case 134:
#line 330 "c--.y"
    { printf("expresion_8 ::= expresion_8 + expresion_9\n"); ;}
    break;

  case 135:
#line 331 "c--.y"
    { printf("expresion_8 ::= expresion_8 - expresion_9\n"); ;}
    break;

  case 136:
#line 332 "c--.y"
    { printf("expresion_8 ::= expresion_9\n"); ;}
    break;

  case 137:
#line 335 "c--.y"
    { printf("expresion_9 ::= expresion_9 * expresion_prefija\n"); ;}
    break;

  case 138:
#line 336 "c--.y"
    { printf("expresion_9 ::= expresion_9 / expresion_prefija\n");  ;}
    break;

  case 139:
#line 337 "c--.y"
    { printf("expresion_9 ::= expresion_9 mod expresion_prefija\n"); ;}
    break;

  case 140:
#line 338 "c--.y"
    { printf("expresion_9 ::= expresion_prefija\n"); ;}
    break;

  case 141:
#line 341 "c--.y"
    { printf("expresion_prefija ::= expresion_postfija\n"); ;}
    break;

  case 142:
#line 342 "c--.y"
    { printf("expresion_prefija ::= sizeof expresion_prefija \n"); ;}
    break;

  case 143:
#line 343 "c--.y"
    { printf("expresion_prefija ::= sizeof(nombre_tipo)\n"); ;}
    break;

  case 144:
#line 344 "c--.y"
    { printf("expresion_prefija ::= ++expresion_prefija\n"); ;}
    break;

  case 145:
#line 345 "c--.y"
    { printf("expresion_prefija ::= --expresion_prefija\n"); ;}
    break;

  case 146:
#line 346 "c--.y"
    { printf("expresion_prefija ::= operador_unario expresion_cast\n"); ;}
    break;

  case 147:
#line 349 "c--.y"
    { printf("expresion_postfija ::= expresion_constante\n"); ;}
    break;

  case 148:
#line 350 "c--.y"
    { printf("expresion_postfija ::= expresion_funcional\n"); ;}
    break;

  case 149:
#line 351 "c--.y"
    { printf("expresion_postfija ::= expresion_indexada\n"); ;}
    break;

  case 150:
#line 352 "c--.y"
    { printf("expresion_postfija ::= expresion_postfija++\n"); ;}
    break;

  case 151:
#line 353 "c--.y"
    { printf("expresion_postfija ::= expresion_postfija--\n"); ;}
    break;

  case 158:
#line 364 "c--.y"
    { printf("expresion_cast ::= expresion_prefija"); ;}
    break;

  case 159:
#line 365 "c--.y"
    { printf("expresion_cast ::= (nombre_tipo) expresion_cast"); ;}
    break;

  case 160:
#line 368 "c--.y"
    { printf("expresion_funcional ::= identificador(<expresion ','>? <expresion>)\n"); ;}
    break;

  case 162:
#line 372 "c--.y"
    { printf("lista_expresiones ::= expresion\n"); ;}
    break;

  case 163:
#line 373 "c--.y"
    { printf("lista_expresion ::= expresion COMA lista_expresiones\n"); ;}
    break;

  case 164:
#line 376 "c--.y"
    { printf("expresion_indexada ::= IDENTIFICADOR\n"); ;}
    break;

  case 165:
#line 377 "c--.y"
    { printf("expresion_indexada ::= expresion_indexada [expresion]\n"); ;}
    break;

  case 166:
#line 378 "c--.y"
    { printf("expresion_indexada ::= expresion_indexada.IDENTIFICADOR\n"); ;}
    break;

  case 167:
#line 379 "c--.y"
    { printf("expresion_indexada ::= expresion_indexada->IDENTIFICADOR\n"); ;}
    break;

  case 168:
#line 382 "c--.y"
    {  printf("expresion_constante ::= constante_real\n");;}
    break;

  case 169:
#line 383 "c--.y"
    { printf("expresion_constante ::= expresion_entera\n"); ;}
    break;

  case 170:
#line 384 "c--.y"
    { printf("expresion_constante ::= constante_caracter\n");;}
    break;

  case 171:
#line 385 "c--.y"
    { printf("expresion_constante ::= constante_cadena\n"); ;}
    break;

  case 172:
#line 386 "c--.y"
    { printf("expresion_constante ::= ( expresion )\n"); ;}
    break;


/* Line 1267 of yacc.c.  */
#line 2463 "c--.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 393 "c--.y"


int main(int argc, char** argv) {
    yyin = fopen(argv[1], "r");
    do{
        yyparse();
    }while(!feof(yyin));
}

yyerror (char *s) { printf ("%s\n", s); }

int yywrap() { return 1; }



