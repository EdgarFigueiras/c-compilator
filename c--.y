%{ /* Codigo C */
#include <stdio.h>
#include <math.h>
extern FILE *yyin;
%}

/* Declaraciones de BISON */
%union {
  int valor_entero;
  double valor_real;
  char * texto;
  char caracter;
}


%token <valor_real> CONSTANTE_REAL
%token <valor_entero> CONSTANTE_ENTERA
%token <caracter> CONSTANTE_CARACTER
%token <texto> CONSTANTE_CADENA
%token <texto> IDENTIFICADOR
%token <texto> LIBRERIA


%token ASIGNACION_IGUAL
%token ASIGNACION_MULT
%token ASIGNACION_DIV
%token ASIGNACION_MOD
%token ASIGNACION_MAS
%token ASIGNACION_MENOS
%token ASIGNACION_DESP_IZQ ASIGNACION_DESP_DER
%token ASIGNACION_AMB
%token ASIGNACION_XOR
%token ASIGNACION_OR


%token DEFINE
%token INCLUDE
%token IF ELSE SWITCH CASE DEFAULT
%token DO WHILE FOR
%token GOTO CONTINUE BREAK
%token RETURN

%token INT SHORT LONG DOUBLE FLOAT CHAR VOID UNSIGNED SIGNED
%token EXTERN STATIC AUTO REGISTER

%token OPERADOR_SIZEOF
%token TYPEDEF
%token SOSTENIDO
%token STRUCT
%token UNION


%token INTERROGANTE
%token COMA
%token PUNTO_Y_COMA DOS_PUNTOS
%token GUSANITO
%token NEGACION_LOG


%token PARIZQ 
%token PARDER
%token CORCHETE_IZQ
%token CORCHETE_DER
%token LLAVE_IZQ LLAVE_DER
%token PUNTO
%token MEMORIA_FLECHA


%token ARITMETICO_MENOSMENOS
%token ARITMETICO_MASMAS
%left OR_LOGICO
%left AND_LOGICO
%left RELACIONAL_IGUAL RELACIONAL_DISTINTO
%left RELACIONAL_MENOR_IGUAL RELACIONAL_MAYOR_IGUAL RELACIONAL_MAYOR RELACIONAL_MENOR
%left OR_BINARIO
%left XOR_BINARIO
%left AND_BINARIO
%left DESP_IZQ DESP_DER
%left SIGNO_MAS SIGNO_MENOS
%left SIGNO_POR SIGNO_DIV SIGNO_MOD
%left NEGATIVO /*Para la negacion unaria*/


%%
/* Gramatica */
programa: 
  | bloques bloque
;

bloques: 
  | bloques bloque
;

bloque: definicion_funcion { printf("bloque ::= definicion_funcion\n"); }
  | declaracion { printf("bloque ::= declaracion\n"); }
  | macros { printf("bloque ::= macros\n"); }
;

definicion_funcion: IDENTIFICADOR bloque_instrucciones { printf("definicion_funcion ::= bloque_instrucciones\n"); }
  | declaracion_tipo lista_asteriscos IDENTIFICADOR bloque_instrucciones { printf("definicion_funcion ::= declaracion_tipo lista_asteriscos IDENTIFICADOR bloque_instrucciones\n"); }
;

macros: SOSTENIDO INCLUDE LIBRERIA
  | SOSTENIDO DEFINE IDENTIFICADOR constante
; 

constante: CONSTANTE_REAL { printf("constante ::= constante_real\n"); }
  | CONSTANTE_ENTERA { printf("constante ::= constante_entera\n"); }
  | CONSTANTE_CADENA { printf("constante ::= constante_cadena\n"); }
  | CONSTANTE_CARACTER { printf("constante ::= constante_caracter\n"); }
;

declaracion:  declaracion_tipo lista_nombres PUNTO_Y_COMA { printf("declaracion ::= declaracion_tipo lista_nombres ;\n"); }
  | declaracion_tipo lista_nombres SOSTENIDO PUNTO_Y_COMA { printf("declaracion ::= declaracion_tipo lista_nombres # ;\n"); }
  | TYPEDEF declaracion_tipo lista_identificador PUNTO_Y_COMA { printf("typedef declaracion_tipo lista_identificador ;\n"); }
;

lista_identificador: IDENTIFICADOR
  | IDENTIFICADOR lista_identificador
;


lista_nombres: 
  | nombre
  | nombre COMA lista_nombres
;

nombre: dato
  | dato ASIGNACION_IGUAL elementos
;


dato: lista_asteriscos IDENTIFICADOR exp_corchetes { printf("lista_asteriscos IDENTIFICADOR exp_corchetes\n"); }
;

exp_corchetes: 
  | CORCHETE_IZQ expresion CORCHETE_DER { printf("exp_corchetes ::= [expresion]\n"); }
  | CORCHETE_IZQ CORCHETE_DER { printf("exp_corchetes ::= []\n"); }
;

elementos: expresion { printf("elementos ::= expresion\n"); }
  | LLAVE_IZQ lista_elementos LLAVE_DER { printf("elementos ::= {lista_elementos}\n"); }
;

lista_elementos: elementos
  | elementos COMA lista_elementos
;

declaracion_tipo: lista_almacenamiento tipo_basico_modificado { printf("declaracion_tipo ::= lista_almacenamiento tipo_basico_modificado\n"); }
  | lista_almacenamiento definicion_struct_union { printf("declaracion_tipo ::= lista_almacenamiento definicion_struct_union\n"); }
;

definicion_struct_union: struct_union LLAVE_IZQ lista_declaracion_struct LLAVE_DER { printf("definicion_struct_union ::= struct_union {lista_declaracion_struct}\n"); }
  | struct_union IDENTIFICADOR LLAVE_IZQ lista_declaracion_struct LLAVE_DER { printf("definicion_struct_union ::= struct_union IDENTIFICADOR {lista_declaracion_struct}\n"); }
  | struct_union IDENTIFICADOR { printf("definicion_struct_union ::= struct_union IDENTIFICADOR\n"); }
;

declaracion_struct: tipo_basico_modificado lista_nombres PUNTO_Y_COMA { printf("declaracion_struct ::= tipo_basico_modificado lista_nombres ;\n"); }
  | definicion_struct_union lista_nombres PUNTO_Y_COMA { printf("declaracion_struct ::= definicion_struct_union lista_nombres ;\n"); }
;

lista_declaracion_struct: declaracion_struct 
  | declaracion_struct lista_declaracion_struct
;

struct_union: STRUCT { printf("struct_union ::= struct\n"); } | UNION { printf("struct_union ::= union\n"); }
;

lista_almacenamiento: 
  | almacenamiento lista_almacenamiento
;

almacenamiento: EXTERN { printf("almacenamiento ::= extern\n"); }
  | STATIC { printf("almacenamiento ::= static\n"); }
  | AUTO { printf("almacenamiento ::= auto\n"); }
  | REGISTER { printf("almacenamiento ::= register\n"); }
;

tipo_basico_modificado: signo longitud tipo_basico
  | CORCHETE_IZQ IDENTIFICADOR CORCHETE_DER
;

tipo_basico: VOID { printf("tipo_basico ::= void\n"); }
  | CHAR { printf("tipo_basico ::= char\n"); }
  | INT { printf("tipo_basico ::= int\n"); }
  | FLOAT { printf("tipo_basico ::= float\n"); }
  | DOUBLE { printf("tipo_basico ::= double\n"); }
;

signo: 
  | SIGNED { printf("signo ::= signed\n"); }
  | UNSIGNED { printf("signo ::= unsigned\n"); }
;

longitud:
  | SHORT { printf("longitud ::= short\n"); }
  | LONG { printf("longitud ::= long\n"); }
;

nombre_tipo: tipo_basico_modificado lista_asteriscos { printf("nombre_tipo ::= tipo_basico_modificado lista_asteriscos\n"); }
;

lista_asteriscos: 
  | SIGNO_POR lista_asteriscos
;


/* ESPECIFICACION DE LA GRAMATICA PARA INSTRUCCIONES */
instruccion: bloque_instrucciones { printf("instruccion ::= bloque_instrucciones\n"); }
  | instruccion_expresion { printf("instruccion ::= instruccion_expresion\n"); }
  | instruccion_bifurcacion { printf("instruccion ::= instruccion_bifurcacion\n"); }
  | instruccion_bucle { printf("instruccion ::= instruccion_bucle\n"); }
  | instruccion_salto { printf("instruccion ::= instruccion_salto\n"); }
  | instruccion_destino_salto { printf("instruccion ::= instruccion_destino_salto\n"); }
  | instruccion_retorno { printf("instruccion ::= instruccion_retorno\n"); }
;

bloque_instrucciones: LLAVE_IZQ lista_declaraciones lista_instrucciones LLAVE_DER { printf("bloque_instrucciones ::= { lista_declaraciones lista_instrucciones }\n"); }
;

lista_declaraciones: 
  | declaracion lista_declaraciones { printf("lista_declaraciones ::= declaracion lista_declaraciones\n"); }
;

lista_instrucciones: instruccion { printf("lista_instrucciones ::= instruccion\n"); }
  | instruccion lista_instrucciones { printf("lista_instrucciones ::= instruccion lista_instrucciones\n"); }
;

instruccion_expresion: expresion_funcional PUNTO_Y_COMA { printf("instruccion_expresion ::= expresion_funcional;\n"); }
  | asignacion PUNTO_Y_COMA { printf("instruccion_expresion ::= asignacion;\n"); }
;

asignacion: expresion_indexada operador_asignacion expresion { printf("asignacion ::= expresion_indexada operador_asignacion expresion\n"); }
;

operador_asignacion: ASIGNACION_IGUAL
  | ASIGNACION_MULT
  | ASIGNACION_DIV
  | ASIGNACION_MOD
  | ASIGNACION_MAS
  | ASIGNACION_MENOS
  | ASIGNACION_DESP_IZQ
  | ASIGNACION_DESP_DER
  | ASIGNACION_AMB
  | ASIGNACION_XOR
  | ASIGNACION_OR
;

instruccion_bifurcacion: IF PARIZQ expresion PARDER instruccion { printf("instruccion_bifurcacion ::= if ( expresion ) instruccion\n"); }
  | IF PARIZQ expresion PARDER instruccion ELSE instruccion { printf("instruccion_bifurcacion ::= if ( expresion ) instruccion else instruccion\n"); }
  | SWITCH PARIZQ expresion PARDER LLAVE_IZQ instruccion_caso LLAVE_DER { printf("instruccion_bifurcacion ::= switch (expresion) {instruccion_caso}\n"); }
; 
/* aqui tenemos el problema del dangling else, que provoca un conflicto salto/reduccion, el cual bison soluciona con su
 * opcion por defecto, reduccion. */

instruccion_caso: CASE expresion DOS_PUNTOS instruccion { printf("instruccion_caso ::= case expresion : instruccion\n"); }
  | DEFAULT DOS_PUNTOS instruccion { printf("instruccion_caso ::= default expresion : instruccion\n"); }
;

instruccion_bucle: WHILE PARIZQ expresion PARDER instruccion { printf("instruccion_bucle ::=  while ( expresion ) instruccion\n"); }
  | DO instruccion WHILE PARIZQ expresion PARDER PUNTO_Y_COMA { printf("instruccion_bucle ::= do instruccion while ( expresion );\n"); }
  | FOR PARIZQ lista_asignaciones PUNTO_Y_COMA expresion PUNTO_Y_COMA expresion PARDER instruccion { printf("for(lista_asignaciones; expresion; expresion) instruccion\n"); }
;

lista_asignaciones: 
  | asignacion { printf("lista_asignaciones ::= asignacion\n"); }
  | asignacion COMA lista_asignaciones { printf("lista_asignaciones ::= asignacion , lista_asignaciones\n"); }
;

instruccion_salto: GOTO IDENTIFICADOR PUNTO_Y_COMA { printf("instruccion_salto ::= GOTO INDENTIFICADOR ;\n"); }
  | CONTINUE PUNTO_Y_COMA { printf("instruccion_salto ::=  continue ;\n"); }
  | BREAK PUNTO_Y_COMA { printf("instruccion_salto ::=  break ;\n"); }
;

instruccion_destino_salto: IDENTIFICADOR DOS_PUNTOS instruccion PUNTO_Y_COMA { printf("IDENTIFICADOR:instruccion;\n"); }
;

instruccion_retorno: RETURN expresion PUNTO_Y_COMA { printf("instruccion_retorno ::= return expresion ;\n"); }
  | RETURN PUNTO_Y_COMA { printf("instruccion_retorno ::= return ;\n"); }
;

/* FIN DE ESPECIFICACION DE LA GRAMATICA PARA INSTRUCCIONES */

/* FUNCIONES Y MACROS */

/* FIN FUNCIONES Y MACROS */

/* ESPECIFICACION DE LA GRAMATICA PARA EXPRESIONES */
expresion: expresion_logica { printf("expresion ::= expresion_logica \n"); }
  | expresion_logica INTERROGANTE expresion DOS_PUNTOS expresion { printf("expresion ::= expresion_logica ? expresion : expresion \n"); }
;

expresion_logica: expresion_logica OR_LOGICO expresion_1 { printf("expresion_logica ::= expresion_logica || expresion_1 \n"); }
  | expresion_1 { printf("expresion_logica ::= expresion_1 \n"); }
;

expresion_1: expresion_1 AND_LOGICO expresion_2 { printf("expresion_1 ::= expresion_1 && expresion_2\n"); }
  | expresion_2 { printf("expresion_1 ::= expresion_2\n"); }
;

expresion_2: expresion_2 RELACIONAL_IGUAL expresion_3 { printf("expresion_2 ::= expresion_2 == expresion_3\n"); }
  | expresion_2 RELACIONAL_DISTINTO expresion_3 { printf("expresion_2 ::= expresion_2 != expresion_3\n"); }
  | expresion_3 { printf("expresion_2 ::= expresion_3\n"); }
;

expresion_3: expresion_3 RELACIONAL_MENOR expresion_4 { printf("expresion_3 ::= expresion_3 < expresion_4\n"); }
  | expresion_3 RELACIONAL_MAYOR expresion_4 { printf("expresion_3 ::= expresion_3 > expresion_4 \n"); }
  | expresion_3 RELACIONAL_MENOR_IGUAL expresion_4 { printf("expresion_3 ::= expresion_3 <= expresion_4\n"); }
  | expresion_3 RELACIONAL_MAYOR_IGUAL expresion_4 { printf("expresion_3 ::= expresion_3 >= expresion_4\n"); }
  | expresion_4 { printf("expresion_3 ::= expresion_4\n"); }
;

expresion_4: expresion_4 OR_BINARIO expresion_5 { printf("expresion_4 ::= expresion_4 | expresion_5\n"); }
  | expresion_5 { printf("expresion_4 ::= expresion_5\n"); }
;

expresion_5: expresion_5 XOR_BINARIO expresion_6 { printf("expresion_5 ::= expresion_5 ^ expresion_6\n"); }
  | expresion_6 { printf("expresion_5 ::= expresion_6\n");}
;

expresion_6: expresion_6 AND_BINARIO expresion_7 { printf("expresion_6 ::= expresion_6 & expresion_7\n"); }
  | expresion_7 { printf("expresion_6 ::= expresion_7\n"); }
;

expresion_7: expresion_7 DESP_IZQ expresion_8 { printf("expresion_7 ::= expresion_7 << expresion_8\n"); }
  | expresion_7 DESP_DER expresion_8 { printf("expresion_7 ::= expresion_7 >> expresion_8\n"); }
  | expresion_8 { printf("expresion_7 ::= expresion_8\n"); }
;

expresion_8: expresion_8 SIGNO_MAS expresion_9 { printf("expresion_8 ::= expresion_8 + expresion_9\n"); }
  | expresion_8 SIGNO_MENOS expresion_9 { printf("expresion_8 ::= expresion_8 - expresion_9\n"); }
  | expresion_9 { printf("expresion_8 ::= expresion_9\n"); }
;

expresion_9: expresion_9 SIGNO_POR expresion_prefija { printf("expresion_9 ::= expresion_9 * expresion_prefija\n"); }
  | expresion_9 SIGNO_DIV expresion_prefija { printf("expresion_9 ::= expresion_9 / expresion_prefija\n");  }
  | expresion_9 SIGNO_MOD expresion_prefija { printf("expresion_9 ::= expresion_9 mod expresion_prefija\n"); }
  | expresion_prefija { printf("expresion_9 ::= expresion_prefija\n"); }
;

expresion_prefija: expresion_postfija { printf("expresion_prefija ::= expresion_postfija\n"); }
  | OPERADOR_SIZEOF expresion_prefija { printf("expresion_prefija ::= sizeof expresion_prefija \n"); }
  | OPERADOR_SIZEOF PARIZQ nombre_tipo PARDER { printf("expresion_prefija ::= sizeof(nombre_tipo)\n"); }
  | ARITMETICO_MASMAS expresion_prefija { printf("expresion_prefija ::= ++expresion_prefija\n"); }
  | ARITMETICO_MENOSMENOS expresion_prefija { printf("expresion_prefija ::= --expresion_prefija\n"); }
  | operador_unario expresion_cast { printf("expresion_prefija ::= operador_unario expresion_cast\n"); }
;

expresion_postfija: expresion_constante { printf("expresion_postfija ::= expresion_constante\n"); }
  | expresion_funcional { printf("expresion_postfija ::= expresion_funcional\n"); }
  | expresion_indexada { printf("expresion_postfija ::= expresion_indexada\n"); }
  | expresion_postfija ARITMETICO_MASMAS { printf("expresion_postfija ::= expresion_postfija++\n"); }
  | expresion_postfija ARITMETICO_MENOSMENOS  { printf("expresion_postfija ::= expresion_postfija--\n"); }
;

operador_unario: AND_BINARIO
  | SIGNO_POR
  | SIGNO_MAS
  | SIGNO_MENOS
  | GUSANITO
  | NEGACION_LOG
;

expresion_cast: expresion_prefija { printf("expresion_cast ::= expresion_prefija"); }
  | PARIZQ nombre_tipo PARDER expresion_cast { printf("expresion_cast ::= (nombre_tipo) expresion_cast"); }
;

expresion_funcional: IDENTIFICADOR PARIZQ lista_expresiones PARDER { printf("expresion_funcional ::= identificador(<expresion ','>? <expresion>)\n"); }
;

lista_expresiones:
  | expresion { printf("lista_expresiones ::= expresion\n"); }
  | expresion COMA lista_expresiones { printf("lista_expresion ::= expresion COMA lista_expresiones\n"); }
;

expresion_indexada: IDENTIFICADOR { printf("expresion_indexada ::= IDENTIFICADOR\n"); }
  | expresion_indexada CORCHETE_IZQ expresion CORCHETE_DER { printf("expresion_indexada ::= expresion_indexada [expresion]\n"); }
  | expresion_indexada PUNTO IDENTIFICADOR { printf("expresion_indexada ::= expresion_indexada.IDENTIFICADOR\n"); }
  | expresion_indexada MEMORIA_FLECHA IDENTIFICADOR { printf("expresion_indexada ::= expresion_indexada->IDENTIFICADOR\n"); }
;

expresion_constante: CONSTANTE_REAL {  printf("expresion_constante ::= constante_real\n");}
  | CONSTANTE_ENTERA   { printf("expresion_constante ::= expresion_entera\n"); }
  | CONSTANTE_CARACTER { printf("expresion_constante ::= constante_caracter\n");}
  | CONSTANTE_CADENA   { printf("expresion_constante ::= constante_cadena\n"); }
  | PARIZQ expresion PARDER { printf("expresion_constante ::= ( expresion )\n"); }
;

/* FIN ESPECIFICACION DE LA GRAMATICA PARA EXPRESIONES */



%%

int main(int argc, char** argv) {
    yyin = fopen(argv[1], "r");
    do{
        yyparse();
    }while(!feof(yyin));
}

yyerror (char *s) { printf ("%s\n", s); }

int yywrap() { return 1; }


