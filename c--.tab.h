/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     CONSTANTE_REAL = 258,
     CONSTANTE_ENTERA = 259,
     CONSTANTE_CARACTER = 260,
     CONSTANTE_CADENA = 261,
     IDENTIFICADOR = 262,
     LIBRERIA = 263,
     ASIGNACION_IGUAL = 264,
     ASIGNACION_MULT = 265,
     ASIGNACION_DIV = 266,
     ASIGNACION_MOD = 267,
     ASIGNACION_MAS = 268,
     ASIGNACION_MENOS = 269,
     ASIGNACION_DESP_IZQ = 270,
     ASIGNACION_DESP_DER = 271,
     ASIGNACION_AMB = 272,
     ASIGNACION_XOR = 273,
     ASIGNACION_OR = 274,
     DEFINE = 275,
     INCLUDE = 276,
     IF = 277,
     ELSE = 278,
     SWITCH = 279,
     CASE = 280,
     DEFAULT = 281,
     DO = 282,
     WHILE = 283,
     FOR = 284,
     GOTO = 285,
     CONTINUE = 286,
     BREAK = 287,
     RETURN = 288,
     INT = 289,
     SHORT = 290,
     LONG = 291,
     DOUBLE = 292,
     FLOAT = 293,
     CHAR = 294,
     VOID = 295,
     UNSIGNED = 296,
     SIGNED = 297,
     EXTERN = 298,
     STATIC = 299,
     AUTO = 300,
     REGISTER = 301,
     OPERADOR_SIZEOF = 302,
     TYPEDEF = 303,
     SOSTENIDO = 304,
     STRUCT = 305,
     UNION = 306,
     INTERROGANTE = 307,
     COMA = 308,
     PUNTO_Y_COMA = 309,
     DOS_PUNTOS = 310,
     GUSANITO = 311,
     NEGACION_LOG = 312,
     PARIZQ = 313,
     PARDER = 314,
     CORCHETE_IZQ = 315,
     CORCHETE_DER = 316,
     LLAVE_IZQ = 317,
     LLAVE_DER = 318,
     PUNTO = 319,
     MEMORIA_FLECHA = 320,
     ARITMETICO_MENOSMENOS = 321,
     ARITMETICO_MASMAS = 322,
     OR_LOGICO = 323,
     AND_LOGICO = 324,
     RELACIONAL_DISTINTO = 325,
     RELACIONAL_IGUAL = 326,
     RELACIONAL_MENOR = 327,
     RELACIONAL_MAYOR = 328,
     RELACIONAL_MAYOR_IGUAL = 329,
     RELACIONAL_MENOR_IGUAL = 330,
     OR_BINARIO = 331,
     XOR_BINARIO = 332,
     AND_BINARIO = 333,
     DESP_DER = 334,
     DESP_IZQ = 335,
     SIGNO_MENOS = 336,
     SIGNO_MAS = 337,
     SIGNO_MOD = 338,
     SIGNO_DIV = 339,
     SIGNO_POR = 340,
     NEGATIVO = 341
   };
#endif
/* Tokens.  */
#define CONSTANTE_REAL 258
#define CONSTANTE_ENTERA 259
#define CONSTANTE_CARACTER 260
#define CONSTANTE_CADENA 261
#define IDENTIFICADOR 262
#define LIBRERIA 263
#define ASIGNACION_IGUAL 264
#define ASIGNACION_MULT 265
#define ASIGNACION_DIV 266
#define ASIGNACION_MOD 267
#define ASIGNACION_MAS 268
#define ASIGNACION_MENOS 269
#define ASIGNACION_DESP_IZQ 270
#define ASIGNACION_DESP_DER 271
#define ASIGNACION_AMB 272
#define ASIGNACION_XOR 273
#define ASIGNACION_OR 274
#define DEFINE 275
#define INCLUDE 276
#define IF 277
#define ELSE 278
#define SWITCH 279
#define CASE 280
#define DEFAULT 281
#define DO 282
#define WHILE 283
#define FOR 284
#define GOTO 285
#define CONTINUE 286
#define BREAK 287
#define RETURN 288
#define INT 289
#define SHORT 290
#define LONG 291
#define DOUBLE 292
#define FLOAT 293
#define CHAR 294
#define VOID 295
#define UNSIGNED 296
#define SIGNED 297
#define EXTERN 298
#define STATIC 299
#define AUTO 300
#define REGISTER 301
#define OPERADOR_SIZEOF 302
#define TYPEDEF 303
#define SOSTENIDO 304
#define STRUCT 305
#define UNION 306
#define INTERROGANTE 307
#define COMA 308
#define PUNTO_Y_COMA 309
#define DOS_PUNTOS 310
#define GUSANITO 311
#define NEGACION_LOG 312
#define PARIZQ 313
#define PARDER 314
#define CORCHETE_IZQ 315
#define CORCHETE_DER 316
#define LLAVE_IZQ 317
#define LLAVE_DER 318
#define PUNTO 319
#define MEMORIA_FLECHA 320
#define ARITMETICO_MENOSMENOS 321
#define ARITMETICO_MASMAS 322
#define OR_LOGICO 323
#define AND_LOGICO 324
#define RELACIONAL_DISTINTO 325
#define RELACIONAL_IGUAL 326
#define RELACIONAL_MENOR 327
#define RELACIONAL_MAYOR 328
#define RELACIONAL_MAYOR_IGUAL 329
#define RELACIONAL_MENOR_IGUAL 330
#define OR_BINARIO 331
#define XOR_BINARIO 332
#define AND_BINARIO 333
#define DESP_DER 334
#define DESP_IZQ 335
#define SIGNO_MENOS 336
#define SIGNO_MAS 337
#define SIGNO_MOD 338
#define SIGNO_DIV 339
#define SIGNO_POR 340
#define NEGATIVO 341




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 8 "c--.y"
{
  int valor_entero;
  double valor_real;
  char * texto;
  char caracter;
}
/* Line 1529 of yacc.c.  */
#line 228 "c--.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

